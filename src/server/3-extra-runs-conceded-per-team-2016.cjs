const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let matches_data_id = matches_data
    .filter((match) => {
        return match.season === '2016';
    })
    .map((match) => {
        return match.id
    });

let extra_runs_per_team= deliveries_data.reduce((accumulator,delivery)=>{

   if(matches_data_id.includes(delivery.match_id))
   {
        const bowling_team=delivery.bowling_team;
        let extra_runs = parseInt(delivery.extra_runs);

        if(!accumulator.hasOwnProperty(bowling_team))
        {
            accumulator[bowling_team]=0;
        }
        else
        {
            accumulator[bowling_team]+=extra_runs;
            
        }
   }

   return accumulator;

},{});

fs.writeFile('../public/output/3-extra-runs-conceded-per-team-2016.json', JSON.stringify(extra_runs_per_team, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});
