const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data= Papa.parse(matches,{header: true}).data;

let match_per_year=matches_data.reduce((accumulator,match)=>{

    let season=match.season;

    if(season===undefined)
    {
        return accumulator;
    }
    if(accumulator.hasOwnProperty(season))
    {
        accumulator[season]+=1;
    }
    else
    {
        accumulator[season]=1;
    }

    return accumulator;
    
},{});

fs.writeFile('../public/output/1-matches-per-year.json',JSON.stringify(match_per_year,null,2),(err)=>{
    if(err)
    {
        console.log("error in saving");
    }
    else{
        console.log("saved successfully");
    }
});