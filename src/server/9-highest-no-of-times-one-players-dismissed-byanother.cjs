const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

const dismissalRecords = deliveries_data.reduce((accumulator, delivery) => {

    const batsman = delivery.batsman;
    const bowler=delivery.bowler;
    const dismissal_kind=delivery.dismissal_kind;

    if (dismissal_kind!=='' && dismissal_kind && batsman && bowler) 
    {
        if (!accumulator.hasOwnProperty(batsman)) 
        {
          accumulator[batsman] = {};
        }
  
        if (!accumulator[batsman].hasOwnProperty(bowler)) 
        {
          accumulator[batsman][bowler] = 0;
        }
  
        accumulator[batsman][bowler] = accumulator[batsman][bowler] + 1;
      }
  
    return accumulator;
    
  }, {});

  let highestDismissals = 0;
  let batsmanWithMostDismissals = null;
  let bowlerWithMostDismissals = null;

  for (const batsman in dismissalRecords) {
    for (const bowler in dismissalRecords[batsman]) {
      const dismissalCount = dismissalRecords[batsman][bowler];
      if (dismissalCount > highestDismissals) {
        highestDismissals = dismissalCount;
        batsmanWithMostDismissals = batsman;
        bowlerWithMostDismissals = bowler;
      }
    }
  }

let result={
    batsman: batsmanWithMostDismissals,
    bowler: bowlerWithMostDismissals,
    dismissals: highestDismissals
  };

fs.writeFile('../public/output/9-highest-no-of-times-one-players-dismissed-byanother.json', JSON.stringify(result, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});