const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let season_no_of_players_of_match = matches_data.reduce((accumulator, match) => {

    let season = match.season;
    let player_of_match = match.player_of_match;

    if (season === undefined || player_of_match==='') 
    {
        return accumulator;
    }
    
    if (!accumulator.hasOwnProperty(season)) 
    {
        accumulator[season] = {};
    }
    else 
    {
        if(!accumulator[season].hasOwnProperty(player_of_match))
        {
            accumulator[season][player_of_match]=1;
        }  
        else{
            accumulator[season][player_of_match]++;
        }  
    }

    return accumulator;

}, {});

let result=Object.keys(season_no_of_players_of_match).map((season)=>{

    const players=season_no_of_players_of_match[season];

    const season_player_top=Object.keys(players).sort((first_player,second_player)=>{
        return players[second_player]-players[first_player]

    })[0];

    return {[season]: season_player_top};
});

fs.writeFile('../public/output/6-player-won-highest-number-of-POM-awards-each-season.json', JSON.stringify(result, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});