const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let season_batsman_record = deliveries_data.reduce((accumulator, delivery) => {

    let batsman=delivery.batsman;
    const batsman_runs = parseInt(delivery.batsman_runs);

    let season_middle=matches_data.find((match)=>{
        return match.id===delivery.match_id
    });

    let season=season_middle ? season_middle.season : "";

    if(!accumulator.hasOwnProperty(season))
    {
        accumulator[season]={};
    }
    if(!accumulator[season].hasOwnProperty(batsman))
    {
        accumulator[season][batsman]={
            runs: batsman_runs,
            ball_faced: 1
        };
    }
    else
    {
        
        accumulator[season][batsman].runs+=batsman_runs;
        accumulator[season][batsman].ball_faced+=1;
    }
    
    return accumulator;

}, {});

let batsman_strike_record= Object.entries(season_batsman_record).reduce((accumulator,[season,batsman_data])=>{

    accumulator[season]={};

    for(batsman_record in batsman_data)
    {
        let {runs,ball_faced}=batsman_data[batsman_record];
        let strke_rate=(runs/ball_faced)*100;
        accumulator[season][batsman_record]=strke_rate.toFixed(2);
    }

    return accumulator;

},{});

fs.writeFile('../public/output/7-strike-rate-of-batsman-each-season.json', JSON.stringify(batsman_strike_record, null, 2), (err) => {
    if (err) 
    {
        console.log("error in saving");
    }
    else 
    {
        console.log("saved successfully");
    }
});
