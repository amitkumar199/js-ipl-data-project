const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let super_over=deliveries_data.filter((delivery)=>{

    return delivery.is_super_over==='1';

})

let bowler_record=super_over.reduce((accumulator,delivery)=>{

    let bowler=delivery.bowler;
    let total_runs=parseInt(delivery.total_runs);
  
   if(delivery.wide_runs==='0' && delivery.noball_runs==='0')
   {
        if(!accumulator.hasOwnProperty(bowler))
        {
            accumulator[bowler]={
                runs: 0,
                balls: 0
            };
            
        } 
        else
        {
            accumulator[bowler].runs+=total_runs;
            accumulator[bowler].balls+=1;
        }
   }

  return accumulator;

},{});

let bowler_record_economy_super_over = Object.keys(bowler_record).map((bowler)=>{

    let {runs,balls}=bowler_record[bowler];
    let economy=(runs/balls)/6;

    return {bowler,economy};

})

fs.writeFile('../public/output/8-bowler-best-economy-super-over.json', JSON.stringify(bowler_record_economy_super_over, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});