const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let toss_team_winner=matches_data.reduce((accumulator,match)=>{
   
    let toss_winner=match.toss_winner;
    let match_winner=match.winner;
    
    if(match.season===undefined)
    {
        return accumulator;
    }
    if(toss_winner===match_winner)
    {
        if(!accumulator.hasOwnProperty(toss_winner))
        {
            accumulator[toss_winner]=1;
        } 
        else
        {
            accumulator[toss_winner]+=1;
        }
    }

  return accumulator;

},{});

fs.writeFile('../public/output/5-number-of-times-each-team-won-the-toss-and-also-won-the-match.json', JSON.stringify(toss_team_winner, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});
