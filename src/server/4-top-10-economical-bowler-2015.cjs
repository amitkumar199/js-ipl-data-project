const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;

let matches_data_id = matches_data
    .filter((match) => {
        if(match.season === '2015' && match.season!==undefined)
        {
            return true;
        }
    })
    .map((match) => {
        
        return match.id
    });

let bowler_record=deliveries_data.reduce((accumulator,delivery)=>{

    let bowler=delivery.bowler; 
    let total_runs=parseInt(delivery.total_runs);
  
   if(matches_data_id.includes(delivery.match_id) && delivery.wide_runs==='0' && delivery.noball_runs==='0')
   {
        if(!accumulator.hasOwnProperty(bowler))
        {
            accumulator[bowler]={
                runs: total_runs,
                balls: 1
            };
            
        } 
        else
        {
            accumulator[bowler].runs+=total_runs;
            accumulator[bowler].balls+=1;
        }
   }

  return accumulator;

},{});

let economic_rate= Object.keys(bowler_record).map((bowler)=>{

    let {runs,balls}=bowler_record[bowler]
    let economical_rate= (runs/balls)*6;

    return {
        bowler,
        economical_rate
    }
});

economic_rate.sort((first,second)=>{
    return first.economical_rate-second.economical_rate;
});

let top_10_economical_bowler=economic_rate.slice(0,10);

fs.writeFile('../public/output/4-top-10-economical-bowler-2015.json', JSON.stringify(top_10_economical_bowler, null, 2), (err) => {
    if (err) {
        console.log("error in saving");
    }
    else {
        console.log("saved successfully");
    }
});