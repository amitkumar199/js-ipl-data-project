const fs = require('fs');
const Papa = require('papaparse');
const deliveries = fs.readFileSync("../data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("../data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data= Papa.parse(matches,{header: true}).data;

let matches_won_per_team_per_year=matches_data.reduce((accumulator,match)=>{

    let season=match.season;
    let winner=match.winner;

    if(season===undefined)
    {
        return accumulator;
    }
    if(!accumulator.hasOwnProperty(season))
    {
        accumulator[season]={};
    }
    else
    {
        if(accumulator[season].hasOwnProperty(winner))
        {
            accumulator[season][winner]+=1;
        }    
        else{
            accumulator[season][winner]=1;
        }
    }

    return accumulator;
    
},{});

fs.writeFile('../public/output/2-matches-won-per-team-per-year.json',JSON.stringify(matches_won_per_team_per_year,null,2),(err)=>{
    if(err)
    {
        console.log("error in saving");
    }
    else{
        console.log("saved successfully");
    }
});